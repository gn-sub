/*
 * This file is part of Gnome Subtitles.
 * Copyright (C) 2007-2008 Pedro Castro
 *
 * Gnome Subtitles is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Gnome Subtitles is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

using GnomeSubtitles.Ui.Edit;
using Mono.Unix;
using SubLib;
using SubLib.Core;
using SubLib.Exceptions;
using System;
using GnomeSubtitles.Dialog;
using GnomeSubtitles.Dialog.Unmanaged;
using Gtk;

namespace GnomeSubtitles.Core.Command {

public class TranslatorCommand : FixedSingleSelectionCommand {
	private string translatedText; // translated string
	private string formerText; // former text replaced by translation
	private bool leftToRight; // direction of translating
	private static string description = Catalog.GetString("Translating text using Google Translate.");
		
	public TranslatorCommand (bool leftToRight) : base(description, false, false) {
		this.translatedText = String.Empty;
		this.formerText = String.Empty;
		this.leftToRight = leftToRight;
			
		SetStopsGrouping(true);
	}
	
	/* Public members */

	public override bool CanGroupWith (Command command) {
		return default(bool);
	}
	
	public override Command MergeWith (Command command) {
		return this;
	}
		
	protected override bool ChangeValues () {
		if (Base.Ui.View.Selection.Count != 1)
			return false;
			
		if (leftToRight)
			formerText = Base.Ui.View.Selection.Subtitle.Translation.Get();
		else
			formerText = Base.Ui.View.Selection.Subtitle.Text.Get();
			
		try
		{
			if (!Base.SpellLanguages.HasActiveTextLanguage)
			{
				Base.Dialogs.Get(typeof(SetTextLanguageDialog)).Show();
				if (!Base.SpellLanguages.HasActiveTextLanguage)
					return false;
			}
			if (!Base.SpellLanguages.HasActiveTranslationLanguage)
			{
				Base.Dialogs.Get(typeof(SetTranslationLanguageDialog)).Show();
				if (!Base.SpellLanguages.HasActiveTranslationLanguage)
					return false;
			}
				
			string translated;
			if (leftToRight)
				translated = Translator.TranslateText(Base.Ui.View.Selection.Subtitle.Text.Get(), /*"en_US", "de_DE",*/ Base.SpellLanguages.ActiveTextLanguage.ID, Base.SpellLanguages.ActiveTranslationLanguage.ID, Translator.TIMEOUT);
			else
				translated = Translator.TranslateText(Base.Ui.View.Selection.Subtitle.Translation.Get(), Base.SpellLanguages.ActiveTextLanguage.ID, Base.SpellLanguages.ActiveTranslationLanguage.ID, Translator.TIMEOUT);
			
			translatedText = translated;
			if (leftToRight)
				Base.Ui.View.Selection.Subtitle.Translation.Set(translated);
			else
				Base.Ui.View.Selection.Subtitle.Text.Set(translated);
			
			return true;
		}
		catch (TranslatorException e)
		{
			Gtk.MessageDialog md = new Gtk.MessageDialog(Base.Ui.Window, 
                                  DialogFlags.DestroyWithParent,
                              	  MessageType.Error, 
                                  ButtonsType.Close, e.Message);	
			md.Run();
			md.Destroy();
			
			return false;
		}
	}
	
	public override void Undo () {
		if (leftToRight)
			Base.Ui.View.Selection.Subtitle.Translation.Set(formerText);
		else
			Base.Ui.View.Selection.Subtitle.Text.Set(formerText);
			
		PostProcess();
	}
	
	public override void Redo () {
		if (leftToRight)
			Base.Ui.View.Selection.Subtitle.Translation.Set(translatedText);
		else
			Base.Ui.View.Selection.Subtitle.Text.Set(translatedText);
			
		PostProcess();
	}
	
	/* Protected members */
	
	protected override void PostProcess () {
		Base.Ui.View.Selection.Reselect();
		Base.Ui.View.Refresh();
		
	}
	
	protected bool GetLeftToRight () {
		return leftToRight;
	}
	
	protected string GetTranslatedText () {
		return translatedText;
	}
	
	protected string GetFormerText () {
		return formerText;
	}

}

}
